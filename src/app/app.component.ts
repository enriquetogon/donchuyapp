import firebase from 'firebase';
import { Component, ViewChild } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { MainMenuTabsPage } from '../pages/main-menu-tabs/main-menu-tabs';
import { LocationPage } from '../pages/location/location';
import { OnboardingPage } from '../pages/onboarding/onboarding';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = LoginPage;
  loginPage = LoginPage;
  signUpPage = SignUpPage;
  mainMenuTabsPage = MainMenuTabsPage;
  onboardingPage = OnboardingPage;
  homePage = HomePage;
  locationPage = LocationPage;
  isAuthenticated = false;
  @ViewChild('nav') nav: NavController;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    firebase.initializeApp({
      apiKey: "AIzaSyA6QLKjLzuMVdkOHl5co_peaVTr8QYBpbA",
      authDomain: "donchuy-55678.firebaseapp.com"
    });
    firebase.auth().onAuthStateChanged(user => {
      if(user){
        this.isAuthenticated = true;
        this.rootPage = this.locationPage;
      }else{
        this.isAuthenticated = false;
        this.rootPage = this.onboardingPage;
      }
    });
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

}

