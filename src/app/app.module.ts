import { TooltipsModule } from 'ionic-tooltips'
import { NgCircleProgressModule } from 'ng-circle-progress';
import { ChartsModule } from 'ng2-charts/charts/charts';
import { AgmCoreModule } from '@agm/core';
import { LottieAnimationViewModule } from 'lottie-angular2';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { AuthService } from '../services/auth';
import { MainMenuTabsPage } from '../pages/main-menu-tabs/main-menu-tabs';
import { LocationPage } from '../pages/location/location';
import { ProfilePage } from '../pages/profile/profile';
import { AchievementPage } from '../pages/achievement/achievement';
import { StoresPage } from '../pages/stores/stores';
import { ProductsPage } from '../pages/products/products';
import { BasketPage } from '../pages/basket/basket';
import { BasketService } from '../services/basket';
import { BasketInfoPage } from '../pages/basket-info/basket-info';
import { BasketVerificationPage } from '../pages/basket-verification/basket-verification';
import { BasketThanksPage } from '../pages/basket-thanks/basket-thanks';
import { StorePage } from '../pages/store/store';
import { StoreService } from '../services/stores';
import { ProductPage } from '../pages/product/product';
import { OnboardingPage } from '../pages/onboarding/onboarding';
import { UserService } from '../services/user';
import { SupportPage } from '../pages/support/support';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SignUpPage,
    MainMenuTabsPage,
    LocationPage,
    ProfilePage,
    AchievementPage,
    StoresPage,
    ProductsPage,
    BasketPage,
    BasketInfoPage,
    BasketVerificationPage,
    BasketThanksPage,
    StorePage,
    ProductPage,
    OnboardingPage,
    SupportPage
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(MyApp,{
     backButtonText: 'Atrás'
    }),
    TooltipsModule,
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#C7E596",
      animationDuration: 300,
    }),
    ChartsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBXOwogHNGLN-IpjU1YZURWbsaxi918EC0'
    }),
    LottieAnimationViewModule.forRoot(),
    HttpClientModule 
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    SignUpPage,
    MainMenuTabsPage,
    LocationPage,
    ProfilePage,
    AchievementPage,
    StoresPage,
    ProductsPage,
    BasketPage,
    BasketInfoPage,
    BasketVerificationPage,
    BasketThanksPage,
    StorePage,
    ProductPage,
    OnboardingPage,
    SupportPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService,
    BasketService,
    StoreService,
    UserService
  ]
})
export class AppModule {}
