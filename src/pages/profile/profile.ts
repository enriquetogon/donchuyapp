import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { AuthService } from '../../services/auth';
import { UserAddress } from '../../data/userAddress.interface';
import testUserAddress from '../../data/testUserAddress';
import { BasketService } from '../../services/basket';
import { BasketPage } from '../basket/basket';
import { UserService } from '../../services/user';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage implements OnInit{
  products: number;
  user: any;
  

  editEnabled: boolean = true;
  selector: string = "comprador";
  address: UserAddress[];

  addressEdition: boolean;
  paymentEdition: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  private authService: AuthService,
  private basketService: BasketService,
  private userService: UserService,
  private modalCtrl: ModalController) {
    this.user =  this.userService.fetchUserData();
    this.addressEdition = true;
    this.paymentEdition = true;
    this.address = testUserAddress;
    //console.log(this.address[0].name);

  }

  ionViewWillEnter(){
    this.products = this.basketService.getAddedProducts().length;
    this.user = this.userService.fetchUserData();
   // console.log(this.user);
    //console.log(this.products);
  }


  ngOnInit(){
    this.getName();
    this.address = testUserAddress;
    //this.user = this.userService.getUserData();

  }

  getName(){
    this.user = this.userService.getUserData();
  }

  onGoBasket(){
    this.basketService.basketClosed(false);
    const basket = this.modalCtrl.create(BasketPage);
    basket.present();
    basket.onDidDismiss(()=>{
     this.products = this.basketService.getAddedProducts().length;
    });
  }

  enableEdit(){
    this.editEnabled = !this.editEnabled;
  }

  enableAddressEdit(){
    this.addressEdition = !this.addressEdition;
  }

  enableEditPayment(){
    this.paymentEdition = !this.paymentEdition;
  }

  onLogout(){
    this.authService.logout();
  }

}
