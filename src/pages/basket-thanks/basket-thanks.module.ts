import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BasketThanksPage } from './basket-thanks';

@NgModule({
  declarations: [
    BasketThanksPage,
  ],
  imports: [
    IonicPageModule.forChild(BasketThanksPage),
  ],
})
export class BasketThanksPageModule {}
