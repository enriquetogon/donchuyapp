import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { BasketService } from '../../services/basket';

@Component({
  selector: 'page-basket-thanks',
  templateUrl: 'basket-thanks.html',
})
export class BasketThanksPage {
  basketService: BasketService;
  public lottieConfig01: Object;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.lottieConfig01 = {
      path: 'animations/fireworks.json',
      autoplay: true,
      loop: true
    };
  }

  onCloseBasket(){
    this.navCtrl.popToRoot();
  }


}
