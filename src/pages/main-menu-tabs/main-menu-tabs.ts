import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';
import { AchievementPage } from '../achievement/achievement';
import { StoresPage } from '../stores/stores';
import { ProductsPage } from '../products/products';

@Component({
  selector: 'page-main-menu-tabs',
  templateUrl: 'main-menu-tabs.html',
})
export class MainMenuTabsPage {
  profilePage = ProfilePage;
  achievementPage = AchievementPage;
  storesPage = StoresPage;
  productsPage = ProductsPage;
  homePage = HomePage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }


}
