import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MainMenuTabsPage } from './main-menu-tabs';

@NgModule({
  declarations: [
    MainMenuTabsPage,
  ],
  imports: [
    IonicPageModule.forChild(MainMenuTabsPage),
  ],
})
export class MainMenuTabsPageModule {}
