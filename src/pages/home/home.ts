import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';

//Dummy data

import { storeCommerce } from '../../data/storeCommerce.interface';
import { Product } from '../../data/product.interface';
import { BasketPage } from '../basket/basket';
import { BasketService } from '../../services/basket';
import { StorePage } from '../store/store';
import { StoreService } from '../../services/stores';
import { UserService } from '../../services/user';
import { ProductPage } from '../product/product';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  basketProducts: number;
  featuredStore: any;
  recommendedProducts: {Product}[];
  testStore: any;

  constructor(public navCtrl: NavController,
              private modalCtrl: ModalController,
              private basketService: BasketService,
              private storeService: StoreService,
              private userService: UserService) {
                this.featuredStore  = this.userService.feauterdStore[0];
    //console.log(this.featuredStore);

  }

  ionViewWillEnter(){
    this.basketProducts = this.basketService.getAddedProducts().length;
    this.userService.getUserData();
    this.featuredStore  = this.userService.feauterdStore[0];
    //console.log(this.featuredStore);
    //this.testStore = this.userService.feauterdStore;
    //console.log(this.testStore);
    //console.log(this.products);
  }

  ngOnInit(){
    //this.featuredStore = featuredStore;
    this.featuredStore  = this.userService.feauterdStore[0];
    //console.log(this.featuredStore);
    
    this.basketProducts = this.basketService.getAddedProducts().length;
    this.recommendedProducts = this.userService.recommendedProducts;
  }


  onGoProduct(selectedProduct){
    const productPage = this.modalCtrl.create(ProductPage,{product: selectedProduct});
    productPage.present();
    productPage.onDidDismiss(()=>{
      this.basketProducts= this.basketService.getAddedProducts().length;
     });
  }

  addProductToBasket(selectedProduct: Product){
   this.basketService.addProductToBasket(selectedProduct);
   this.basketProducts = this.basketService.getAddedProducts().length;
  }

  addStore(selectedStore: storeCommerce){
    this.storeService.addStoreToView(selectedStore);
   }

  //To Display Basket Modal
  onGoBasket(){
    this.basketService.basketClosed(false);
    const basket = this.modalCtrl.create(BasketPage);
    basket.present();
    basket.onDidDismiss(()=>{
     this.basketProducts = this.basketService.getAddedProducts().length;
    });
  }

  onGoStore(selectedStore){
    const storePage = this.modalCtrl.create(StorePage);
    storePage.present();
    storePage.onDidDismiss(()=>{
      this.basketProducts = this.basketService.getAddedProducts().length;
     });
  }

  //Bar chart stuff

  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels:string[] = ['Miel'];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
 
  public barChartData:any[] = [
    {data: [26], label: 'San Ramón'},
    {data: [68], label: 'Tres Cruces'},
    {data: [20], label: 'Prados Agua'}
  ];
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 


}
