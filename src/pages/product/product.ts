import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, ViewController, ToastController } from 'ionic-angular';
import { Location } from '../../models/location';
import { storeCommerce } from '../../data/storeCommerce.interface';
import { StoreService } from '../../services/stores';
import { DomSanitizer } from '@angular/platform-browser';
import { BasketService } from '../../services/basket';
import { Product } from '../../data/product.interface';


@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {
  location: Location = {
    lat: 18.975858,
    lng: -98.219609
  }

  users: Array<any> = [
    {
      profilePicture: 'imgs/users/user01.jpg',
      name: 'Alesito',
      rank: 'Baby Chuy',
      review: 'Está excelente'
    },{
      profilePicture: 'imgs/users/user02.jpg',
      name: 'Moi Muñoz',
      rank: 'Elder Chuy',
      review: 'Buenísimo'
    },{
      profilePicture: 'imgs/users/user03.jpg',
      name: 'Jorge Naveda',
      rank: 'King Chuy',
      review: '2 que 3'
    },{
      profilePicture: 'imgs/users/user04.jpg',
      name: 'Omar Dorantes',
      rank: 'Master Chuy',
      review: 'Me re-encanta'
    },
  ];


  selector: string = "General";

  @ViewChild(Content)
  content:Content;
  doc: any; 
  isScrolled = false;
  currPos: Number = 0;
  startPos: Number = 0;
  changePos: Number = 20;

  productOnView: Product;
  progressGoal: number = 10;
  progressTotal: number = 1;

  imageHeader: string;

  

  constructor(public navCtrl: NavController, public navParams: NavParams,
  private viewCtrl: ViewController,
  private storeService: StoreService,
  private _sanitizer: DomSanitizer,
  private basketService: BasketService,
  private toastCtrl: ToastController) {
    this.productOnView = this.navParams.data.product;
    this.imageHeader = this.productOnView.imagePath;
   
  }

  getBackground(image) {
    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
}

addProductToBasket(selectedProduct: Product){
  this.basketService.addProductToBasket(selectedProduct);
  //this.products= this.basketService.getAddedProducts().length;
  this.presentToast(selectedProduct);
 }

 presentToast(product) {
  let toast = this.toastCtrl.create({
    message: product.name + ' agregado a la canasta :)',
    duration: 2000
  });
  toast.present();
}


  ionViewWillEnter(){
    this.productOnView = this.navParams.data.product;
    console.log(this.productOnView);

  }

  onCloseBasket(){
    this.viewCtrl.dismiss();
  }


//Bar chart stuff

/*

public barChartOptions:any = {
  scaleShowVerticalLines: false,
  responsive: true,
  scales: {
    yAxes: [
     {
         display: false,
         ticks: {
          beginAtZero:true,max:30
         }
     }
   ],
   xAxes: [{
    display: false,
    ticks: {
      beginAtZero:true,max:20
     }
            }]
 },
  labels:{
    fontColor: '#fff'
  }
};
public barChartLabels:string[] = ['Dinero'];
public barChartType:string = 'horizontalBar';
public barChartLegend:boolean = false;

public barChartData:any[] = [
  {data: [this.progressGoal]}
];
public barChartStoreData:any[] = [
  {data: [this.progressTotal]}
];

public chartColors: Array<any> = [
  { // first color
    backgroundColor: 'rgba(77,94,104,0.9)',
    borderColor: 'rgba(77,94,104,0)',
    pointBackgroundColor: 'rgba(77,94,104,0.2)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(77,94,104,0.9)'
  }
];

public chartStoreColors: Array<any> = [
  { // first color
    backgroundColor: 'rgba(247,138,154,0.9)',
    borderColor: 'rgba(247,138,154,0)',
    pointBackgroundColor: 'rgba(7247,138,154,0.2)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(247,138,154,0.9)'
  }
];

// events
public chartClicked(e:any):void {
  console.log(e);
}

*/


  
//All code below is for updating the content size in order to viewd, thera are still some issues
  ngAfterViewInit() {
    this.content.ionScroll.subscribe((evt) =>{
      this.currPos = (window.pageYOffset || evt.scrollTop) - (evt.clientTop || 0);
      this.doc = document.getElementById('header').offsetHeight;
      if(this.currPos >= this.changePos ) {
        this.isScrolled = true;
        this.content.resize();
    } else {
        this.isScrolled = false; 
    }
      if(this.content.isScrolling){
        this.content.resize();
      }
      //console.log(this.content.getContentDimensions().contentTop);
    });
    this.content.ionScrollEnd.subscribe((evt)=>{
      this.content.resize();
      if(this.doc <= this.content.getContentDimensions().contentTop) {
        
        this.content.resize();
    } else if(this.content.getContentDimensions().contentTop <= this.doc){
      this.content.resize();
    }
    });
}

onResizeHeader(){
  this.content.resize();
}

}
