import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MainMenuTabsPage } from '../main-menu-tabs/main-menu-tabs';
import { UserService } from '../../services/user';

@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class LocationPage  {
  mainMenuTabsPage: MainMenuTabsPage;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  private userService: UserService) {
  }

  goToFactsPage(){
    this.navCtrl.push(MainMenuTabsPage,{});
    }

    ionViewWillEnter(){
      this.userService.getFeauteredStore();
      this.userService.getRecommendedProducts();
      this.userService.getRegularStores();
      this.userService.getProducts();
      //console.log(this.products);
    }
    
}
