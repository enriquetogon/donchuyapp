import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BasketInfoPage } from './basket-info';

@NgModule({
  declarations: [
    BasketInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(BasketInfoPage),
  ],
})
export class BasketInfoPageModule {}
