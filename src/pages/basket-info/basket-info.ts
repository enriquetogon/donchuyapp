import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Product } from '../../data/product.interface';
import { BasketPage } from '../basket/basket';

import { UserAddress } from '../../data/userAddress.interface';
import testUserAddress from '../../data/testUserAddress';
import { BasketVerificationPage } from '../basket-verification/basket-verification';

@Component({
  selector: 'page-basket-info',
  templateUrl: 'basket-info.html',
})
export class BasketInfoPage implements OnInit{
  basketPage = BasketPage;
  basketVerificationPage = BasketVerificationPage;
  basketProducts: Product[];
  dateSet: string = "06/2012";

  userAddress: UserAddress[];

  addressEdition: boolean;
  paymentEdition: boolean;
  creditCardSelected: boolean;
  chuyMoneySelected: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.basketProducts = this.navParams.data;
    this.addressEdition = true;
    this.paymentEdition = true;
    this.creditCardSelected = true;
    this.chuyMoneySelected = false;
    //console.log(this.basketProducts);
  }

  

  ngOnInit(){
    this.userAddress = testUserAddress;
  }

  enableEdit(){
    this.addressEdition = false;
  }
  enableEditPayment(){
    this.paymentEdition = false;
  }

  selectPayment(){
    this.creditCardSelected = !this.creditCardSelected;
    this.chuyMoneySelected = !this.chuyMoneySelected;
  }

  goBack(){
    this.navCtrl.push(this.basketPage,{},{animate: true, direction: 'back'});
  }

}
