import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { SignUpPage } from '../sign-up/sign-up';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  signUpPage = SignUpPage;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private authService: AuthService,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController) {}

  onLogin(form: NgForm){
    const loading = this.loadingCtrl.create({
      content: 'Entrando a Don Chuy :)'
    });
    loading.present();
    this.authService.login(form.value.email, form.value.password)
      .then(data => {
        loading.dismiss();
      })
      .catch(error => {
        loading.dismiss();
        const errorToast = this.toastCtrl.create({
          message: error.message,
          showCloseButton: true,
          closeButtonText: 'Entiendo'
        });
        errorToast.present();
      });
  }


}
