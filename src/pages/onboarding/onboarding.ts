import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides } from 'ionic-angular';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-onboarding',
  templateUrl: 'onboarding.html',
})
export class OnboardingPage {
  LoginPage = LoginPage;
  public lottieConfig01: Object;
  public lottieConfig02: Object;
  public lottieConfig03: Object;
  private anim: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.lottieConfig01 = {
      path: 'animations/whys.json',
      autoplay: true,
      loop: true
    };

    this.lottieConfig02 = {
      path: 'animations/like.json',
      autoplay: true,
      loop: true
    };

    this.lottieConfig03 = {
      path: 'animations/confetti.json',
      autoplay: true,
      loop: true
    };
  }

  @ViewChild(Slides) slides: Slides;

  skip(){
    this.slides.slideTo(2, 700, true);
  }

  handleAnimation(anim: any) {
    this.anim = anim;
  }

}
