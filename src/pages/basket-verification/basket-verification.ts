import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

import { Product } from '../../data/product.interface';
import { BasketService } from '../../services/basket';
import { BasketThanksPage } from '../basket-thanks/basket-thanks';

@Component({
  selector: 'page-basket-verification',
  templateUrl: 'basket-verification.html',
})
export class BasketVerificationPage {

basketThanksPage: BasketThanksPage;

basketProductsConfirmed: Product[];
paymentMethod: boolean;
totalPay: any;
test:any;
balance: number;

paymentEnough: boolean;
cantPay: boolean;

totalF(){
let total = 0;
  for(let i = 0; i < this.basketProductsConfirmed.length; i++){
      total += parseInt(this.navParams.data.basketProducts[i].price) * (this.navParams.data.basketProducts[i].amount);
     
  }

  return this.test = total;

}

calculateBalance(){
  if(this.totalPay < this.balance){
    this.paymentEnough = false;
  } else{
    this.paymentEnough = true;
  }
}

letThemPay(){
  if(this.paymentEnough == true && this.paymentMethod == true){
    this.cantPay = true;
  }else{
    this.cantPay = false;
  }
  //console.log(this.cantPay);
}
  constructor(public navCtrl: NavController, public navParams: NavParams,
  private viewCtrl: ViewController, private basketService: BasketService) {
    this.basketProductsConfirmed = this.navParams.data.basketProducts;
    this.paymentMethod = this.navParams.data.chuyMoneySelected;
    this.totalPay = this.totalF();
    this.balance = 85;
    this.cantPay = false;
    this.calculateBalance();
    this.letThemPay();

    //console.log(this.paymentMethod);
    

    //console.log((this.basketProductsConfirmed.length)-1);
    //console.log(this.paymentMethod);

  }

  onCloseBasket(){
    this.basketService.basketClosed(true);
    this.navCtrl.popToRoot();
  }

  onPay(){
    this.basketService.removeAllProductsFromBasket(this.basketProductsConfirmed);
    this.navCtrl.push(BasketThanksPage);
  }




}
