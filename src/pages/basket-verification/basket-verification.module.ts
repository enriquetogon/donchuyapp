import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BasketVerificationPage } from './basket-verification';

@NgModule({
  declarations: [
    BasketVerificationPage,
  ],
  imports: [
    IonicPageModule.forChild(BasketVerificationPage),
  ],
})
export class BasketVerificationPageModule {}
