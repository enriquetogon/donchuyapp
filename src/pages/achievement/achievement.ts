import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { BasketService } from '../../services/basket';
import { BasketPage } from '../basket/basket';


@Component({
  selector: 'page-achievement',
  templateUrl: 'achievement.html',
})
export class AchievementPage {
  selector: string = "comprador";
  constructor(public navCtrl: NavController, public navParams: NavParams,
  private basketService: BasketService, 
  private modalCtrl: ModalController) {
  }

  basketProducts: number;

  progressGoal: number = 10;
  progressTotal: number = 1;

  achievements: Array<any> = [
    {
      name: 'Autoretrato',
      icon: 'happy',
      description: 'Pon una foto de perfil para que la gente se deleite con tu lindo rostro :)',
      progress: 0,
      totalProgress: 0
    },
    {
      name: 'El Rey del Oro Café',
      icon: 'cafe',
      description: 'Compra productos con chocolate o cacao más de 5 ocasiones en un lapso de menos de 2 semanas.',
      progress: 0,
      totalProgress: 5
    },
    {
      name: 'Explorador Novato',
      icon: 'compass',
      description: 'Has comprado productos de más de 6 tiendas diferentes.',
      progress: 0,
      totalProgress: 6
    },{
      name: 'Paso a Pasito',
      icon: 'walk',
      description: 'Visitaste y escaneaste el código de una tienda donde ya has pedido.',
      progress: 0,
      totalProgress: 1
    },{
      name: 'Doña Pelos aprueba esto',
      icon: 'thumbs-up',
      description: 'Compraste un producto directamente desde una tienda ambulante que registra menos de 50 ventas.',
      progress: 0,
      totalProgress: 1
    },{
      name: 'El Popu de la Cuadra',
      icon: 'people',
      description: 'Compartiste una de tus compras en tus redes sociales.',
      progress: 0,
      totalProgress: 1
    },{
      name: 'Vale Verdura',
      icon: 'nutrition',
      description: 'Hiciste una compra de más de 300 pesos en una sola compra en frutas y verduras.',
      progress: 0,
      totalProgress: 1
    }
  ]

  ionViewWillEnter(){
    this.basketProducts = this.basketService.getAddedProducts().length;
    //console.log(this.basketProducts);
  }

    //To Display Basket Modal
    onGoBasket(){
      this.basketService.basketClosed(false);
      const basket = this.modalCtrl.create(BasketPage);
      basket.present();
      basket.onDidDismiss(()=>{
       this.basketProducts = this.basketService.getAddedProducts().length;
      });
    }

  //Bar chart stuff

  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels:string[] = ['Miel'];
  public barChartType:string = 'horizontalBar';
  public barChartLegend:boolean = true;
 
  public barChartData:any[] = [
    {data: [24], label: 'San Ramón'},
    {data: [68], label: 'Tres Cruces'},
    {data: [10], label: 'Prados Agua'}
  ];
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
 


}
