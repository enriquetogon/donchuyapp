import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Product } from '../../data/product.interface';
import { BasketService } from '../../services/basket';
import { BasketInfoPage } from '../basket-info/basket-info';
import { UserService } from '../../services/user';

@Component({
  selector: 'page-basket',
  templateUrl: 'basket.html',
})
export class BasketPage implements OnInit{
  basketInfoPage = BasketInfoPage;
  basketProducts: Product[];
  recommendedProducts: {Product}[];

  purchaseDone: boolean;


  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private viewCtrl: ViewController,
    private basketService: BasketService,
    private userService: UserService) {
    
  }

  ngOnInit(){
    this.recommendedProducts = this.userService.recommendedProducts;
  }

  addProductToBasket(selectedProduct: Product){
    this.basketService.addProductToBasket(selectedProduct);
    this.basketProducts.push(selectedProduct);
   }



  ionViewWillEnter(){
    this.basketProducts = this.basketService.getAddedProducts();
    this.purchaseDone = this.basketService.getBasketState();
    if(this.purchaseDone == true){
      this.viewCtrl.dismiss();
    }
    console.log(this.purchaseDone);
  }


  removeBasketProduct(basketProduct: Product){
   this.basketService.removeProductFromBasket(basketProduct);
   const position = this.basketProducts.findIndex((productEl: Product) =>{
      return productEl.id == basketProduct.id;
   });


   this.basketProducts.splice(position,1);
  }

  addAmount(basketProduct: Product){
    
    basketProduct["amount"] = basketProduct["amount"] + 1;

   }

   decreaseAmount(basketProduct: Product){
    
    basketProduct["amount"] = basketProduct["amount"] - 1;
    if(basketProduct["amount"] <= 0){
      this.removeBasketProduct(basketProduct);
    }

   }





  

  onCloseBasket(){
    this.viewCtrl.dismiss();
  }

}
