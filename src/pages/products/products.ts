import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { BasketService } from '../../services/basket';

import regularProductsData from '../../data/regularProducts';
import { BasketPage } from '../basket/basket';

import { Product } from '../../data/product.interface';
import { ProductPage } from '../product/product';
import { UserService } from '../../services/user';

@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
})
export class ProductsPage {

  basketpage: BasketPage;
  regularProducts: any[];

  products: number;
  basketProducts: number;

  searchQuery: string = '';

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private basketService: BasketService, 
    private modalCtrl: ModalController,
    private userService: UserService) {
  }

  ngOnInit(){
    this.regularProducts = this.userService.regularProducts;
    this.basketProducts = this.basketService.getAddedProducts().length;
  }

  ionViewWillEnter(){
    this.products = this.basketService.getAddedProducts().length;
    //console.log(this.products);
  }

  onGoBasket(){
    this.basketService.basketClosed(false);
    const basket = this.modalCtrl.create(BasketPage);
    basket.present();
    basket.onDidDismiss(()=>{
     this.products = this.basketService.getAddedProducts().length;
    });
  }

  onGoProduct(selectedProduct){
    const productPage = this.modalCtrl.create(ProductPage,{product: selectedProduct});
    productPage.present();
    productPage.onDidDismiss(()=>{
      this.products = this.basketService.getAddedProducts().length;
     });
  }

  addProductToBasket(selectedProduct: Product){
    this.basketService.addProductToBasket(selectedProduct);
    this.products= this.basketService.getAddedProducts().length;
   }

   initializeItems() {
    this.regularProducts = this.userService.regularProducts;
  }

   getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.regularProducts= this.regularProducts.filter((product) => {
        return (product.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }

  }


}
