import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {

  loginPage = LoginPage;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  private authService: AuthService,
  private loadingCtrl: LoadingController,
  private toastCtrl: ToastController,
  private http: HttpClient) {}

  goToFactsPage(){
    this.navCtrl.push(this.loginPage,{},{animate: true, direction: 'back'});
    }

    onSignup(form: NgForm){
      const loading = this.loadingCtrl.create({
       content: 'Estamos registrandote en Don Chuy :)'
      });
      loading.present();
      this.authService.signup(form.value.name, form.value.lastname, form.value.title,
        form.value.birthday, form.value.email, form.value.password)
        .then(data => {
          //Sends the rest of the data
          this.authService.getActiveUser().getToken().then(
           (token:string) => {
            const userId = this.authService.getActiveUser().uid;
            //console.log(token);
            //console.log(userId);
            return this.http
            .put('https://donchuy-55678.firebaseio.com/' + userId + '/userInfo.json?auth=' + token, 
            { 
              name: form.value.name,
              lastname: form.value.lastname,
              title: form.value.title,
              birthday: form.value.birthday,
            })
            .subscribe(
              () => console.log('Success'),
              error => {console.log(error);}
            );
           }
          );
 
          //Success
          loading.dismiss();


        })
        .catch(error => {
          //error
          loading.dismiss();
          const errorToast = this.toastCtrl.create({
            message: error.message,
            showCloseButton: true,
            closeButtonText: 'Entiendo'
          });
          errorToast.present();
        });
    }

}
