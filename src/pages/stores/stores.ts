import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController } from 'ionic-angular';

import regularStores from '../../data/regularStores';
import { BasketService } from '../../services/basket';
import { Product } from '../../data/product.interface';
import { BasketPage } from '../basket/basket';
import { StorePage } from '../store/store';
import { StoreService } from '../../services/stores';
import { UserService } from '../../services/user';

@Component({
  selector: 'page-stores',
  templateUrl: 'stores.html',
})
export class StoresPage implements OnInit{
  basketpage: BasketPage;
  stores: any[];
  products: number;
  basketProducts: Product[];

  constructor(public navCtrl: NavController, public navParams: NavParams,
  private basketService: BasketService, private viewCtrl: ViewController, 
  private modalCtrl: ModalController,
  private storeService: StoreService,
  private userService: UserService) {
  }

  ngOnInit(){
    this.initializeItems();
    this.stores = this.userService.regularStores;
    this.basketProducts = this.basketService.getAddedProducts();
  }

  ionViewWillEnter(){
    this.products = this.basketService.getAddedProducts().length;
    console.log(this.products);
  }

  onGoBasket(){
    this.basketService.basketClosed(false);
    const basket = this.modalCtrl.create(BasketPage);
    basket.present();
    basket.onDidDismiss(()=>{
     this.products= this.basketService.getAddedProducts().length;
    });
  }

  addStore(selectedStore: any){
    this.storeService.addStoreToView(selectedStore);
   }

  onGoStore(selectedStore){
    const storePage = this.modalCtrl.create(StorePage);
    storePage.present();
    storePage.onDidDismiss(()=>{
      this.products = this.basketService.getAddedProducts().length;
     });
  }

  initializeItems() {
    this.stores = this.userService.regularStores;
    //console.log(this.stores);
  }

   getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.stores= this.stores.filter((stores) => {
        return (stores.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }

  }

}
