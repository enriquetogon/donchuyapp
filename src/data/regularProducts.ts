export default [
    {
        id: 1,
        name: "Jalea orgánica",
        seller: 'Jalea "Don José"',
        description: 'Hecha con fresas frescas',
        longDescription: 'La mermelada de fresa es un alimento que no contiene proteínas, contiene 62,60 gramos de carbohidratos, contiene 62,60 gramos de azúcar por cada 100 gramos y no tiene grasa, aportando 255,04 calorias a la dieta. Entre sus nutrientes también se encuentran las vitaminas B9, C, A y K.',
        units: '250 ml',
        category: 'Jaleas',
        amount: 1,
        price: 39.00,
        imagePath: '../../assets/imgs/products/prod01.jpg',
        likes: 22
    },{
        id: 2,
        name: "Queso Oaxaca",
        seller: 'Rebaño Cowi',
        units: '1 kg',
        category: 'Lacteos',
        description: 'Cool',
        longDescription: 'El queso, en general, por su origen es una buena fuente de calcio y fósforo, necesarios para la remineralización ósea, de vitaminas liposolubles (A y D) y de vitaminas del complejo B',
        amount: 1,
        price: 49.00,
        imagePath: '../../assets/imgs/products/prod02.jpg',
        likes: 74
    },{
        id: 3,
        name: "Helado de naranja",
        seller: 'Ximitl',
        units: '100 ml',
        category: 'Lacteos',
        amount: 1,
        description: 'Cool',
        longDescription: 'Una dieta sana, debe ser una dieta variada, equilibrada y suficientemente. Dentro de este contexto, podemos consumir y disfrutar del helado sin que afecte negativamente el peso corporal y la salud.',
        price: 21.00,
        imagePath: '../../assets/imgs/products/prod03.jpg',
        likes: 12
    },{
        id: 4,
        name: "Churros caseros",
        seller: 'Pan la Flor de Puebla',
        units: '1 unidad',
        category: 'Pan',
        amount: 1,
        description: 'Calientitos y crujientes',
        longDescription: 'Los churros no solamente son buenos por lo que tienen sino también por lo que no tienen: no tienen colesterol, ni grasas “malas” como las trans, ni azúcar (salvo si queremos añadirlo después), ni ningún tipo de aditivo como colorantes, conservantes, etc',
        price: 3.00,
        imagePath: '../../assets/imgs/products/prod04.jpg',
        likes: 41
    },{
        id: 5,
        name: "Grano de café",
        seller: 'Pan la Flor de Puebla',
        description: 'Tostado. Sembrado en Puebla.',
        longDescription: 'El café es una de las bebidas más consumidas a nivel mundial y hoy se sabe, que tiene grandes propiedades para la salud debido a su nivel de antioxidantes.',
        units: '250 g',
        category: 'Café',
        amount: 1,
        price: 59.00,
        imagePath: '../../assets/imgs/products/prod05.jpg',
        likes: 192
    },{
        id: 6,
        name: "Epazote",
        seller: 'Heboristeria Angélica',
        units: '100 g',
        category: 'Plantas',
        description: 'Sembrado en invernadero casero',
        longDescription: 'El epazote es una hierba aromática, que en las cantidades que se consume no supone un aporte importante de calorías ni macronutrientes (proteínas, hidratos de carbono, grasas, fibra). ... El epazote o paico es una hierba emenagoga, es decir, que facilita la menstruación.',
        amount: 1,
        price: 12.00,
        imagePath: '../../assets/imgs/products/prod06.jpg',
        likes: 14
    },{
        id: 7,
        name: "Jamón serrano",
        seller: 'Carnicería Folgado',
        units: '100 g',
        category: 'Carne',
        amount: 1,
        description: 'Fresquesito y rico',
        longDescription: 'El jamón serrano no contiene hidratos de carbono y sus calorías derivan principalmente de las proteínas de buena calidad que posee, pues el aporte de grasas no es significativo, ya que cada 100 gramos el jamón serrano no llega a los 6 gramos de grasa. Además, el jamón serrano puede poseer grasas de buena calidad en su composición, por eso a continuación te mostramos el tipo de grasas que ofrece este alimento tan sabroso y tan consumido en nuestro país.',
        price: 30.00,
        imagePath: '../../assets/imgs/products/prod07.jpg',
        likes: 46
    },{
        id: 8,
        name: "Leche Santa Clara",
        seller: 'Miscelanea Don Pedro',
        units: '1 lt',
        category: 'Lacteos',
        amount: 1,
        description: 'Cool',
        longDescription: 'La leche es un alimento indispensable de la dieta equilibrada ya que contiene las vitaminas, proteínas y minerales necesarios para el desarrollo de los seres humanos. Aporta energía por su alto contenido en lactosa y grasa y es la fuente alimentaria más importante de calcio.',
        price: 3.00,
        imagePath: '../../assets/imgs/products/prod08.jpg',
        likes: 41
    }
]