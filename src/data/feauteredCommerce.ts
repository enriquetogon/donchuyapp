export default 
    {
        name: "La Abeja Reina",
        description: 'La miel más fresca de Pragos Agua Azul. Fundada por María y Luis en...',
        imagePath: '../../assets/imgs/stores/misc01.jpg',
        headerImage: '../../assets/imgs/storeDetail/owners01.jpg',
        goalProgress: 2,
        weeklyPrice: '100ml de miel obscura gratis',
        weeklyPriceDescription: 'Si vienes a nuestra tienda esta semana puedes llevarte ',
        currentGoal: 'Queremos comprar mesitas para poder ponerlas afuera del local y la gente pueda llegar a relajarse y probar un poco de miel ¿Te gustaría ayudarnos?',
        medals: [
            
                {
                    icon: 'jet',
                    medalName: 'Servicio rápido'
                },{
                    icon: 'planet',
                    medalName: '¡Fuera de este mundo!'
                },{
                    icon: 'basketball',
                    medalName: 'Lugar divertido :)'
                },
                {
                    icon: 'bowtie',
                    medalName: 'Elegante como un pingüino'
                }
            
        ],
        colaborators:[
                    {
                        name: 'María',
                        profilePicture: '../../assets/imgs/storeDetail/owners01/01.jpg',
                        role: 'Tendera/Apicultora',
                        bio:"Nací en Texcoco en los 50's. Ay que bella época. Allí mi abuelo me enseñó a cuidar abejas y poco a poco la apicultura se volvió un pasatiempo maravilloso. Me mudé al aquel entonces D.F. en el 85 y conocí a mi señor, mi Wicho. Todo el tiempo hablamos de mudarnos a un lugar más tranquilo y abrieramos un negocio y aquí estamos, cumpliendo nuestro sueño, ojalá se les cumpla el suyo también. ¡Los esperámos con mucho gusto!"
                    },
                    {
                        name: 'Luis',
                        role: 'Tendero',
                        profilePicture: '../../assets/imgs/storeDetail/owners01/02.jpg',
                        bio:"¡Hola! Soy Don Luis y y atiendo a La Abeja Reina junto con mi adorada esposa Marí. Los dos siempre soñamos con poner nuestro propio negocio y atenderlo ¡Y lo hicimos realidad!"
                    }
        ],
        products:[
            {
                id: 1,
                name: "Jalea orgánica",
                seller: 'Jalea "Don José"',
                description: 'Hecha con fresas frescas',
                units: '250 ml',
                category: 'Jaleas',
                amount: 1,
                price: 39.00,
                imagePath: '../../assets/imgs/products/prod01.jpg',
                likes: 22
            },{
                id: 2,
                name: "Queso Oaxaca",
                seller: 'Rebaño Cowi',
                units: '1 kg',
                category: 'Lacteos',
                description: 'Cool',
                amount: 1,
                price: 49.00,
                imagePath: '../../assets/imgs/products/prod02.jpg',
                likes: 74
            },{
                id: 3,
                name: "Helado de naranja",
                seller: 'Ximitl',
                units: '100 ml',
                category: 'Lacteos',
                amount: 1,
                description: 'Cool',
                price: 21.00,
                imagePath: '../../assets/imgs/products/prod03.jpg',
                likes: 12
            },{
                id: 4,
                name: "Churros caseros",
                seller: 'Pan la Flor de Puebla',
                units: '1 unidad',
                category: 'Pan',
                amount: 1,
                description: 'Cool',
                price: 3.00,
                imagePath: '../../assets/imgs/products/prod04.jpg',
                likes: 41
            },{
                id: 5,
                name: "Grano de café",
                seller: 'Pan la Flor de Puebla',
                description: 'Tostado. Sembrado en Puebla.',
                units: '250 g',
                category: 'Café',
                amount: 1,
                price: 59.00,
                imagePath: '../../assets/imgs/products/prod05.jpg',
                likes: 192
            }
        ]
    }
