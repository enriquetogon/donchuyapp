export interface UserAddress {
    id: number,
    name: string,
    street: string,
    neighborhood: string,
    externalNo: string,
    interiorNo: string,
    city: string,
    state: string,
    country: string,
    cp: number
}