export interface Product {
    id: number,
    name: string,
    seller: string,
    units: string,
    category: string,
    description: string,
    longDescription: Array<any>,
    amount: number,
    price: number,
    imagePath: string,
    likes: number
}
