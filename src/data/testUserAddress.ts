export default [
    {
        id: 1,
        name: 'Mi ubicación',
        street: '18 A Sur',
        neighborhood: 'Prados Agua Azul',
        externalNo: '5126',
        interiorNo: '',
        city: 'Puebla',
        state: 'Puebla',
        country: 'México',
        cp: 72450
    }
]