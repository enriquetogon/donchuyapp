import { Product } from "./product.interface";

export interface storeCommerce {
    id: number,
    name: string,
    description: string,
    imagePath: string,
    currentGoal: string,
    goalProgress: number,
    headerImage: string,
    weeklyPrice: string,
    weeklyPriceDescription: string,
    colaborators: Array<any>,
    medals: Array<any>,
    products: Product
}