export default [
    {
        name: "La Abeja Reina",
        description: 'La miel más fresca de Pragos Agua Azul. Fundada por María y Luis en...',
        imagePath: '../../assets/imgs/stores/misc01.jpg',
        headerImage: '../../assets/imgs/storeDetail/owners01.jpg',
        goalProgress: 2,
        weeklyPrice: '100ml de miel obscura gratis',
        weeklyPriceDescription: 'Si vienes a nuestra tienda esta semana puedes llevarte ',
        currentGoal: 'Queremos comprar mesitas para poder ponerlas afuera del local y la gente pueda llegar a relajarse y probar un poco de miel ¿Te gustaría ayudarnos?',
        medals: [
            
                {
                    icon: 'jet',
                    medalName: 'Servicio rápido'
                },{
                    icon: 'planet',
                    medalName: '¡Fuera de este mundo!'
                },{
                    icon: 'basketball',
                    medalName: 'Lugar divertido :)'
                },
                {
                    icon: 'bowtie',
                    medalName: 'Elegante como un pingüino'
                }
            
        ],
        colaborators:[
            {
                name: 'María',
                profilePicture: '../../assets/imgs/storeDetail/owners01/01.jpg',
                role: 'Tendera/Apicultora',
                bio:"Nací en Texcoco en los 50's. Ay que bella época. Allí mi abuelo me enseñó a cuidar abejas y poco a poco la apicultura se volvió un pasatiempo maravilloso. Me mudé al aquel entonce D.F. en el 85 y conocí a mi señor, mi Wicho. Todo el tiempo hablamos de mudarnos a un lugar más tranquilo y abrieramos un negocio y aquí estamos, cumpliendo nuestro sueño, ojalá se les cumpla el suyo también. ¡Los esperámos con mucho gusto!"
            },
            {
                name: 'Luis',
                role: 'Tendero',
                profilePicture: '../../assets/imgs/storeDetail/owners01/02.jpg',
                bio:"¡Hola! Soy Don Luis y y atiendo a La Abeja Reina junto con mi adorada esposa Marí. Los dos siempre soñamos con poner nuestro propio negocio y atenderlo ¡Y lo hicimos realidad!"
            }
        ],
        products:[
            {
                id: 1,
                name: "Jalea orgánica",
                seller: 'Jalea "Don José"',
                description: 'Hecha con fresas frescas',
                units: '250 ml',
                category: 'Jaleas',
                amount: 1,
                price: 39.00,
                imagePath: '../../assets/imgs/products/prod01.jpg',
                likes: 22
            },{
                id: 2,
                name: "Queso Oaxaca",
                seller: 'Rebaño Cowi',
                units: '1 kg',
                category: 'Lacteos',
                description: 'Cool',
                amount: 1,
                price: 49.00,
                imagePath: '../../assets/imgs/products/prod02.jpg',
                likes: 74
            },{
                id: 3,
                name: "Helado de naranja",
                seller: 'Ximitl',
                units: '100 ml',
                category: 'Lacteos',
                amount: 1,
                description: 'Cool',
                price: 21.00,
                imagePath: '../../assets/imgs/products/prod03.jpg',
                likes: 12
            },{
                id: 4,
                name: "Churros caseros",
                seller: 'Pan la Flor de Puebla',
                units: '1 unidad',
                category: 'Pan',
                amount: 1,
                description: 'Cool',
                price: 3.00,
                imagePath: '../../assets/imgs/products/prod04.jpg',
                likes: 41
            },{
                id: 5,
                name: "Grano de café",
                seller: 'Pan la Flor de Puebla',
                description: 'Tostado. Sembrado en Puebla.',
                units: '250 g',
                category: 'Café',
                amount: 1,
                price: 59.00,
                imagePath: '../../assets/imgs/products/prod05.jpg',
                likes: 192
            }
        ]
    },
    {
        name: "Heboristeria Angélica",
        description: 'Plantas medicinales para la vida. Fundada por...',
        imagePath: '../../assets/imgs/stores/misc02.jpg',
        headerImage: '../../assets/imgs/storeDetail/owners02.jpg',
        goalProgress: 2,
        weeklyPrice: '5 semillas del ermitaño gratis',
        weeklyPriceDescription: 'Si vienes a nuestra tienda esta semana puedes llevarte ',
        currentGoal: 'Reciclamos botellas de plástico, trasnformandolas en macetas para nuestras plantas ¿Te gustaría aportar?',
        medals: [
            
               {
                    icon: 'planet',
                    medalName: '¡Fuera de este mundo!'
                },
                {
                    icon: 'bowtie',
                    medalName: 'Elegante como un pingüino'
                }
            
        ],
        colaborators:[
            {
                name: 'Angélica',
                profilePicture: '../../assets/imgs/storeDetail/owners02/01.jpg',
                role: 'Tendera/Agricultora de patio',
                bio:"La medicina natural cambió mi vida. Cuando tenía 8 años tuve una enfermedad que ningún médico se atrevía a tratar, hasta que uno de ellos me recomendó una receta hecha con plantas medicinales y gracias a ello estoy viva. Desde ese momento decidí poner al alcance de la gente lo que yo no tuve en ese entonces "
            },
            {
                name: 'Alberto',
                role: 'Tendero/Agricultor de patio',
                profilePicture: '../../assets/imgs/storeDetail/owners02/02.jpg',
                bio:"¡Hola! soy Alberto y soy egresado de la UNAM. Estudié inegnieria agrónoma y he decidido enfocarme al uso de técnicas agricultoras en espacios urbanos. Conocí a Angie hace 3 años y decidimos poner este negocio juntos, que creemos le ayudará a mucha gente. Te esperamos con las puetas abiertas :)"
            }
        ],
        products:[
            {
                id: 1,
                name: "Jalea orgánica",
                seller: 'Jalea "Don José"',
                description: 'Hecha con fresas frescas',
                units: '250 ml',
                category: 'Jaleas',
                amount: 1,
                price: 39.00,
                imagePath: '../../assets/imgs/products/prod01.jpg',
                likes: 22
            },{
                id: 2,
                name: "Queso Oaxaca",
                seller: 'Rebaño Cowi',
                units: '1 kg',
                category: 'Lacteos',
                description: 'Cool',
                amount: 1,
                price: 49.00,
                imagePath: '../../assets/imgs/products/prod02.jpg',
                likes: 74
            },{
                id: 3,
                name: "Helado de naranja",
                seller: 'Ximitl',
                units: '100 ml',
                category: 'Lacteos',
                amount: 1,
                description: 'Cool',
                price: 21.00,
                imagePath: '../../assets/imgs/products/prod03.jpg',
                likes: 12
            },{
                id: 4,
                name: "Churros caseros",
                seller: 'Pan la Flor de Puebla',
                units: '1 unidad',
                category: 'Pan',
                amount: 1,
                description: 'Cool',
                price: 3.00,
                imagePath: '../../assets/imgs/products/prod04.jpg',
                likes: 41
            },{
                id: 5,
                name: "Grano de café",
                seller: 'Pan la Flor de Puebla',
                description: 'Tostado. Sembrado en Puebla.',
                units: '250 g',
                category: 'Café',
                amount: 1,
                price: 59.00,
                imagePath: '../../assets/imgs/products/prod05.jpg',
                likes: 192
            }
        ]
    },
    {
        name: "Carnicería Folgado",
        description: 'Cortes exquisitos. Negocio familiar que comenzó con la...',
        imagePath: '../../assets/imgs/stores/misc03.jpg',
        headerImage: '../../assets/imgs/storeDetail/owners03.jpg',
        goalProgress: 2,
        currentGoal: 'Queremos comprar mesitas para poder ponerlas afuera del local y la gente pueda llegar a relajarse y probar un poco de miel ¿Te gustaría ayudarnos?',
        medals: [
            
                {
                    icon: 'jet',
                    medalName: 'Servicio rápido'
                },
            
        ],
        colaborators:[
            {
                name: 'María',
                profilePicture: '../../assets/imgs/storeDetail/owners01/01.jpg',
                role: 'Tendera/Apicultora',
                bio:"Nací en Texcoco en los 50's. Ay que bella época. Allí mi abuelo me enseñó a cuidar abejas y poco a poco la apicultura se volvió un pasatiempo maravilloso. Me mudé al aquel entonce D.F. en el 85 y conocí a mi señor, mi Wicho. Todo el tiempo hablamos de mudarnos a un lugar más tranquilo y abrieramos un negocio y aquí estamos, cumpliendo nuestro sueño, ojalá se les cumpla el suyo también. ¡Los esperámos con mucho gusto!"
            },
            {
                name: 'Luis',
                role: 'Tendero',
                profilePicture: '../../assets/imgs/storeDetail/owners01/02.jpg',
                bio:"¡Hola! Soy Don Luis y y atiendo a La Abeja Reina junto con mi adorada esposa Marí. Los dos siempre soñamos con poner nuestro propio negocio y atenderlo ¡Y lo hicimos realidad!"
            }
        ],
        products:[
            {
                id: 1,
                name: "Jalea orgánica",
                seller: 'Jalea "Don José"',
                description: 'Hecha con fresas frescas',
                units: '250 ml',
                category: 'Jaleas',
                amount: 1,
                price: 39.00,
                imagePath: '../../assets/imgs/products/prod01.jpg',
                likes: 22
            },{
                id: 2,
                name: "Queso Oaxaca",
                seller: 'Rebaño Cowi',
                units: '1 kg',
                category: 'Lacteos',
                description: 'Cool',
                amount: 1,
                price: 49.00,
                imagePath: '../../assets/imgs/products/prod02.jpg',
                likes: 74
            },{
                id: 3,
                name: "Helado de naranja",
                seller: 'Ximitl',
                units: '100 ml',
                category: 'Lacteos',
                amount: 1,
                description: 'Cool',
                price: 21.00,
                imagePath: '../../assets/imgs/products/prod03.jpg',
                likes: 12
            },{
                id: 4,
                name: "Churros caseros",
                seller: 'Pan la Flor de Puebla',
                units: '1 unidad',
                category: 'Pan',
                amount: 1,
                description: 'Cool',
                price: 3.00,
                imagePath: '../../assets/imgs/products/prod04.jpg',
                likes: 41
            },{
                id: 5,
                name: "Grano de café",
                seller: 'Pan la Flor de Puebla',
                description: 'Tostado. Sembrado en Puebla.',
                units: '250 g',
                category: 'Café',
                amount: 1,
                price: 59.00,
                imagePath: '../../assets/imgs/products/prod05.jpg',
                likes: 192
            }
        ]
    },
    {
        name: "Mauro y sus frutas",
        description: 'Frutas frescas y sanas. Mauro siempre ha tenido un gusto por...',
        imagePath: '../../assets/imgs/stores/misc04.jpg',
        headerImage: '../../assets/imgs/storeDetail/owners01.jpg',
        goalProgress: 2,
        currentGoal: 'Queremos comprar mesitas para poder ponerlas afuera del local y la gente pueda llegar a relajarse y probar un poco de miel ¿Te gustaría ayudarnos?',
        medals: [
            
                {
                    icon: 'jet',
                    medalName: 'Servicio rápido'
                },
                {
                    icon: 'basketball',
                    medalName: 'Lugar divertido :)'
                }
            
        ],
        colaborators:[
            {
                name: 'María',
                profilePicture: '../../assets/imgs/storeDetail/owners01/01.jpg',
                role: 'Tendera/Apicultora',
                bio:"Nací en Texcoco en los 50's. Ay que bella época. Allí mi abuelo me enseñó a cuidar abejas y poco a poco la apicultura se volvió un pasatiempo maravilloso. Me mudé al aquel entonce D.F. en el 85 y conocí a mi señor, mi Wicho. Todo el tiempo hablamos de mudarnos a un lugar más tranquilo y abrieramos un negocio y aquí estamos, cumpliendo nuestro sueño, ojalá se les cumpla el suyo también. ¡Los esperámos con mucho gusto!"
            },
            {
                name: 'Luis',
                role: 'Tendero',
                profilePicture: '../../assets/imgs/storeDetail/owners01/02.jpg',
                bio:"¡Hola! Soy Don Luis y y atiendo a La Abeja Reina junto con mi adorada esposa Marí. Los dos siempre soñamos con poner nuestro propio negocio y atenderlo ¡Y lo hicimos realidad!"
            }
        ],
        products:[
            {
                id: 1,
                name: "Jalea orgánica",
                seller: 'Jalea "Don José"',
                description: 'Hecha con fresas frescas',
                units: '250 ml',
                category: 'Jaleas',
                amount: 1,
                price: 39.00,
                imagePath: '../../assets/imgs/products/prod01.jpg',
                likes: 22
            },{
                id: 2,
                name: "Queso Oaxaca",
                seller: 'Rebaño Cowi',
                units: '1 kg',
                category: 'Lacteos',
                description: 'Cool',
                amount: 1,
                price: 49.00,
                imagePath: '../../assets/imgs/products/prod02.jpg',
                likes: 74
            },{
                id: 3,
                name: "Helado de naranja",
                seller: 'Ximitl',
                units: '100 ml',
                category: 'Lacteos',
                amount: 1,
                description: 'Cool',
                price: 21.00,
                imagePath: '../../assets/imgs/products/prod03.jpg',
                likes: 12
            },{
                id: 4,
                name: "Churros caseros",
                seller: 'Pan la Flor de Puebla',
                units: '1 unidad',
                category: 'Pan',
                amount: 1,
                description: 'Cool',
                price: 3.00,
                imagePath: '../../assets/imgs/products/prod04.jpg',
                likes: 41
            },{
                id: 5,
                name: "Grano de café",
                seller: 'Pan la Flor de Puebla',
                description: 'Tostado. Sembrado en Puebla.',
                units: '250 g',
                category: 'Café',
                amount: 1,
                price: 59.00,
                imagePath: '../../assets/imgs/products/prod05.jpg',
                likes: 192
            }
        ]
    },
    {
        name: "Pan la Flor de Puebla",
        description: 'Pan fresquesito. Después de muchos años Omar y su hermano decidieron...',
        imagePath: '../../assets/imgs/stores/misc05.jpg',
        headerImage: '../../assets/imgs/storeDetail/owners01.jpg',
        goalProgress: 2,
        currentGoal: 'Queremos comprar mesitas para poder ponerlas afuera del local y la gente pueda llegar a relajarse y probar un poco de miel ¿Te gustaría ayudarnos?',
        medals: [
            
                {
                    icon: 'planet',
                    medalName: '¡Fuera de este mundo!'
                },{
                    icon: 'basketball',
                    medalName: 'Lugar divertido :)'
                }
            
        ],
        colaborators:[
            {
                name: 'María',
                profilePicture: '../../assets/imgs/storeDetail/owners01/01.jpg',
                role: 'Tendera/Apicultora',
                bio:"Nací en Texcoco en los 50's. Ay que bella época. Allí mi abuelo me enseñó a cuidar abejas y poco a poco la apicultura se volvió un pasatiempo maravilloso. Me mudé al aquel entonce D.F. en el 85 y conocí a mi señor, mi Wicho. Todo el tiempo hablamos de mudarnos a un lugar más tranquilo y abrieramos un negocio y aquí estamos, cumpliendo nuestro sueño, ojalá se les cumpla el suyo también. ¡Los esperámos con mucho gusto!"
            },
            {
                name: 'Luis',
                role: 'Tendero',
                profilePicture: '../../assets/imgs/storeDetail/owners01/02.jpg',
                bio:"¡Hola! Soy Don Luis y y atiendo a La Abeja Reina junto con mi adorada esposa Marí. Los dos siempre soñamos con poner nuestro propio negocio y atenderlo ¡Y lo hicimos realidad!"
            }
        ],
        products:[
            {
                id: 1,
                name: "Jalea orgánica",
                seller: 'Jalea "Don José"',
                description: 'Hecha con fresas frescas',
                units: '250 ml',
                category: 'Jaleas',
                amount: 1,
                price: 39.00,
                imagePath: '../../assets/imgs/products/prod01.jpg',
                likes: 22
            },{
                id: 2,
                name: "Queso Oaxaca",
                seller: 'Rebaño Cowi',
                units: '1 kg',
                category: 'Lacteos',
                description: 'Cool',
                amount: 1,
                price: 49.00,
                imagePath: '../../assets/imgs/products/prod02.jpg',
                likes: 74
            },{
                id: 3,
                name: "Helado de naranja",
                seller: 'Ximitl',
                units: '100 ml',
                category: 'Lacteos',
                amount: 1,
                description: 'Cool',
                price: 21.00,
                imagePath: '../../assets/imgs/products/prod03.jpg',
                likes: 12
            },{
                id: 4,
                name: "Churros caseros",
                seller: 'Pan la Flor de Puebla',
                units: '1 unidad',
                category: 'Pan',
                amount: 1,
                description: 'Cool',
                price: 3.00,
                imagePath: '../../assets/imgs/products/prod04.jpg',
                likes: 41
            },{
                id: 5,
                name: "Grano de café",
                seller: 'Pan la Flor de Puebla',
                description: 'Tostado. Sembrado en Puebla.',
                units: '250 g',
                category: 'Café',
                amount: 1,
                price: 59.00,
                imagePath: '../../assets/imgs/products/prod05.jpg',
                likes: 192
            }
        ]
    },
    {
        name: "Ximitl",
        description: 'Refrescate con una nieve. Una familia proveniente de...',
        imagePath: '../../assets/imgs/stores/misc06.jpg',
        headerImage: '../../assets/imgs/storeDetail/owners01.jpg',
        goalProgress: 2,
        currentGoal: 'Queremos comprar mesitas para poder ponerlas afuera del local y la gente pueda llegar a relajarse y probar un poco de miel ¿Te gustaría ayudarnos?',
        medals: [
            
                {
                    icon: 'jet',
                    medalName: 'Servicio rápido'
                },{
                    icon: 'planet',
                    medalName: '¡Fuera de este mundo!'
                },{
                    icon: 'basketball',
                    medalName: 'Lugar divertido :)'
                },
                {
                    icon: 'bowtie',
                    medalName: 'Elegante como un pingüino'
                }
            
        ],
        colaborators:[
            {
                name: 'María',
                profilePicture: '../../assets/imgs/storeDetail/owners01/01.jpg',
                role: 'Tendera/Apicultora',
                bio:"Nací en Texcoco en los 50's. Ay que bella época. Allí mi abuelo me enseñó a cuidar abejas y poco a poco la apicultura se volvió un pasatiempo maravilloso. Me mudé al aquel entonce D.F. en el 85 y conocí a mi señor, mi Wicho. Todo el tiempo hablamos de mudarnos a un lugar más tranquilo y abrieramos un negocio y aquí estamos, cumpliendo nuestro sueño, ojalá se les cumpla el suyo también. ¡Los esperámos con mucho gusto!"
            },
            {
                name: 'Luis',
                role: 'Tendero',
                profilePicture: '../../assets/imgs/storeDetail/owners01/02.jpg',
                bio:"¡Hola! Soy Don Luis y y atiendo a La Abeja Reina junto con mi adorada esposa Marí. Los dos siempre soñamos con poner nuestro propio negocio y atenderlo ¡Y lo hicimos realidad!"
            }
        ],
        products:[
            {
                id: 1,
                name: "Jalea orgánica",
                seller: 'Jalea "Don José"',
                description: 'Hecha con fresas frescas',
                units: '250 ml',
                category: 'Jaleas',
                amount: 1,
                price: 39.00,
                imagePath: '../../assets/imgs/products/prod01.jpg',
                likes: 22
            },{
                id: 2,
                name: "Queso Oaxaca",
                seller: 'Rebaño Cowi',
                units: '1 kg',
                category: 'Lacteos',
                description: 'Cool',
                amount: 1,
                price: 49.00,
                imagePath: '../../assets/imgs/products/prod02.jpg',
                likes: 74
            },{
                id: 3,
                name: "Helado de naranja",
                seller: 'Ximitl',
                units: '100 ml',
                category: 'Lacteos',
                amount: 1,
                description: 'Cool',
                price: 21.00,
                imagePath: '../../assets/imgs/products/prod03.jpg',
                likes: 12
            },{
                id: 4,
                name: "Churros caseros",
                seller: 'Pan la Flor de Puebla',
                units: '1 unidad',
                category: 'Pan',
                amount: 1,
                description: 'Cool',
                price: 3.00,
                imagePath: '../../assets/imgs/products/prod04.jpg',
                likes: 41
            },{
                id: 5,
                name: "Grano de café",
                seller: 'Pan la Flor de Puebla',
                description: 'Tostado. Sembrado en Puebla.',
                units: '250 g',
                category: 'Café',
                amount: 1,
                price: 59.00,
                imagePath: '../../assets/imgs/products/prod05.jpg',
                likes: 192
            }
        ]
    }
]