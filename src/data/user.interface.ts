export interface User {
    id: number,
    name: string,
    lastname: string,
    title: string,
    birthday: string
}