import { storeCommerce } from "../data/storeCommerce.interface";

export class StoreService {
    private selectedStore: storeCommerce [] = [];
    private purchaseFinished: boolean;

    addStoreToView(store: storeCommerce){
        this.selectedStore.push(store);
    }

    removeStoreToView(store: storeCommerce){
        const position = this.selectedStore.findIndex((productEl :storeCommerce) => {
            return productEl.id == store.id;
        });
        this.selectedStore.splice(position, 1);
        //console.log(this.addedProducts);
    }

    removeAllStoresFromView(stores: any){
        this.selectedStore.splice(0, stores.length);
    }

    getAddedStore(){
        return this.selectedStore.slice();
    }

    basketClosed(closed: boolean){
        this.purchaseFinished = closed;
    }

    getBasketState(){
        return this.purchaseFinished;
    }
}