import { Product } from "../data/product.interface";

export class BasketService {
    private addedProducts: Product [] = [];
    private purchaseFinished: boolean;

    addProductToBasket(product: Product){
        this.addedProducts.push(product);
    }

    removeProductFromBasket(product: Product){
        const position = this.addedProducts.findIndex((productEl :Product) => {
            return productEl.id == product.id;
        });
        this.addedProducts.splice(position, 1);
        //console.log(this.addedProducts);
    }

    removeAllProductsFromBasket(products: any){
        this.addedProducts.splice(0, products.length);
    }

    getAddedProducts(){
        return this.addedProducts.slice();
    }

    basketClosed(closed: boolean){
        this.purchaseFinished = closed;
    }

    getBasketState(){
        return this.purchaseFinished;
    }
}