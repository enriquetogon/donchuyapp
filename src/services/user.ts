import { HttpClient } from "@angular/common/http";
import { AuthService } from "./auth";
import { Injectable } from "@angular/core";
import { storeCommerce } from "../data/storeCommerce.interface";

@Injectable()
export class UserService {
    constructor( private http: HttpClient, 
        private authService: AuthService) {}

        private userData: any;

        public feauterdStore: any;

        public recommendedProducts: any;

        public regularStores: any;

        public regularProducts: any;

    getUserData(){
        //Request user data
        this.authService.getActiveUser().getToken().then(
            (token:string) => {
             const userId = this.authService.getActiveUser().uid;
             //console.log(token);
             //console.log(userId);
             return this.http
             .get('https://donchuy-55678.firebaseio.com/' + userId + '/userInfo.json?auth=' + token)
             .subscribe(
               (user: any) => {
                   //console.log('Success');
                   //console.log(user);
                this.userData = user;

                },
               error => {console.log(error);}
             );
            }
           );


           return this.userData;
    }

    fetchUserData(){
        return this.userData;
    }

    getFeauteredStore(){
        this.authService.getActiveUser().getToken().then(
            (token:string) => {
             const userId = this.authService.getActiveUser().uid;
             //console.log(token);
             //console.log(userId);
             return this.http
             .get('https://donchuy-55678.firebaseio.com/feauturedStore.json?auth=' + token)
             .subscribe(
               (featured: any) => {
                   //console.log('Success');
                   //console.log(user);
                   this.feauterdStore = [featured];
                   //console.log(this.feauterdStore);
                   //return this.feauterdStore;
                
                //console.log(this.feauterdStore );

                },
               error => {console.log(error);}
             );
            }
            
           );     

    }

    getRegularStores(){
        this.authService.getActiveUser().getToken().then(
            (token:string) => {
             const userId = this.authService.getActiveUser().uid;
             //console.log(token);
             //console.log(userId);
             return this.http
             .get('https://donchuy-55678.firebaseio.com/regularStores.json?auth=' + token)
             .subscribe(
               (stores: any) => {
                   //console.log('Success');
                   //console.log(user);
                   this.regularStores = stores;
                   //console.log(this.feauterdStore);
                   //return this.feauterdStore;
                
                //console.log(this.feauterdStore );

                },
               error => {console.log(error);}
             );
            }
            
           ); 
    }

    getProducts(){
        this.authService.getActiveUser().getToken().then(
            (token:string) => {
             const userId = this.authService.getActiveUser().uid;
             //console.log(token);
             //console.log(userId);
             return this.http
             .get('https://donchuy-55678.firebaseio.com/regularProducts.json?auth=' + token)
             .subscribe(
               (products: any) => {
                   console.log('Success');
                   //console.log(user);
                   this.regularProducts = products;
                   //console.log(this.recommendedProducts);
                   //return this.feauterdStore;
                
                //console.log(this.feauterdStore );

                },
               error => {console.log(error);}
             );
            }
            
           );     

    }


    getRecommendedProducts(){

        this.authService.getActiveUser().getToken().then(
            (token:string) => {
             const userId = this.authService.getActiveUser().uid;
             //console.log(token);
             //console.log(userId);
             return this.http
             .get('https://donchuy-55678.firebaseio.com/recommendedProducts.json?auth=' + token)
             .subscribe(
               (products: any) => {
                   //console.log('Success');
                   //console.log(user);
                   this.recommendedProducts = products;
                   //console.log(this.recommendedProducts);
                   //return this.feauterdStore;
                
                //console.log(this.feauterdStore );

                },
               error => {console.log(error);}
             );
            }
            
           );     

    }



}