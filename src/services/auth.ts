import firebase from 'firebase';

export class AuthService {
    signup(name: string, lastname: string, title: string, birthday: string, email: string, password: string){
        return firebase.auth().createUserWithEmailAndPassword(email, password);
    }

    login(email: string, password: string){
        return firebase.auth().signInWithEmailAndPassword(email, password);
    }

    logout(){
       firebase.auth().signOut();
    }

    getActiveUser(){
       return firebase.auth().currentUser;
    }
}